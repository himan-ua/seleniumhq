package integration.test;


import integration.configuration.IntegrationBase;
import integration.pagemodels.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SeleniumTest extends IntegrationBase {
    private HomePage homePage;
    private ProjectsPage projectsPage;
    private DownloadPage downloadPage;
    private DocumentationPage documentationPage;
    private SupportPage supportPage;
    private AboutPage aboutPage;

    //variables
    String seleniumHeaderText = "What is Selenium?";
    String projectsHeaderText = "Selenium Projects";
    String downloadHeaderText = "Downloads";
    String documentationHeaderText = "Selenium Documentation";
    String supportHeaderText = "Getting Help";
    String aboutHeaderText = "About Selenium";

    String supportBodyText1 = "User Group";
    String supportBodyText2 = "Chat Room";
    String supportBodyText3 = "Bug Tracker";
    String supportBodyText4 = "Commercial Support";

    public SeleniumTest(String browser) {
        super(browser);
    }

    @BeforeMethod
    public void setupHomePagetext() {
        homePage = new HomePage(driver);
        projectsPage = new ProjectsPage(driver);
        downloadPage = new DownloadPage(driver);
        documentationPage = new DocumentationPage (driver);
        supportPage = new SupportPage(driver);
        aboutPage = new AboutPage(driver);
        openBasicHomePage(BASIC_URL);
    }

    @Test
    public void homePageTest() {
        homePage.waitForPageToLoad();
        homePage.verifyWhatIsSeleniumTextIsPresent(seleniumHeaderText);
    }

    @Test
    public void seleniumProjectsPageTest() {
        homePage.waitForPageToLoad();
        homePage.clickOnProjectsTitle();
        projectsPage.verifySeleniumProjectsTextIsPresent(projectsHeaderText);
    }

    @Test
    public void downloadPageTest() {
        homePage.waitForPageToLoad();
        homePage.clickOnDownloadTitle();
        downloadPage.verifyDownloadsTextIsPresent(downloadHeaderText);
    }

    @Test
    public void seleniumDocumentationPageTest() {
        homePage.waitForPageToLoad();
        homePage.clickOnDocumentationTitle();
        documentationPage.verifySeleniumDocumentationTextIsPresent(documentationHeaderText);
    }

    @Test
    public void gettingHelpPageTest() {
        homePage.waitForPageToLoad();
        homePage.clickOnSupportTitle();
        supportPage.verifyGettingHelpTextIsPresent(supportHeaderText);
    }

    @Test
    public void gettingBodyText1Test() {
        homePage.waitForPageToLoad();
        homePage.clickOnSupportTitle();
        supportPage.verifyUserGroupTextIsPresent(supportBodyText1);
    }

    @Test
    public void gettingBodyText2Test() {
        homePage.waitForPageToLoad();
        homePage.clickOnSupportTitle();
        supportPage.verifyChatRoomTextIsPresent(supportBodyText2);
    }

    @Test
    public void gettingBodyText3Test() {
        homePage.waitForPageToLoad();
        homePage.clickOnSupportTitle();
        supportPage.verifyBugTrackerTextIsPresent(supportBodyText3);
    }

    @Test
    public void gettingBodyText4Test() {
        homePage.waitForPageToLoad();
        homePage.clickOnSupportTitle();
        supportPage.verifyCommercialSupportTextIsPresent(supportBodyText4);
    }

    @Test
    public void aboutSeleniumPageTest() {
        homePage.waitForPageToLoad();
        homePage.clickOnAboutTitle();
        aboutPage.verifyAboutSeleniumTextIsPresent(aboutHeaderText);
    }
}
