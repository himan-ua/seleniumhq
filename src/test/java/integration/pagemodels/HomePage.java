package integration.pagemodels;

import integration.configuration.AbstractWebComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

public class HomePage extends AbstractWebComponent {
    //locators
    @FindBy(id = "footer")
    private WebElement siteFooter;

    @FindBy(css = "#mainContent h2:nth-child(1)")
    private WebElement headerText;

    @FindBy(css = "a[title='Selenium Projects']")
    private WebElement seleniumProjectsTitle;

    @FindBy(css = "#selenium-documentation>h1")
    private WebElement headerText1;

    @FindBy(css = "a[title='Technical references and guides']")
    private WebElement seleniumDocumentationTitle;

    @FindBy(css = "#mainContent>h1")
    private WebElement headerText2;

    @FindBy(css = "#menu_support>a")
    private WebElement GettingHelpTitle;

    @FindBy(css = "#mainContent h2:nth-child(1)")
    private WebElement headerText3;

    @FindBy(css = "#menu_about>a")
    private WebElement AboutSeleniumTitle;

    @FindBy(css = "#mainContent h2:nth-child(1)")
    private WebElement headerText4;

    @FindBy(css = "#menu_download>a")
    private WebElement DownloadsTitle;


    public HomePage(WebDriver driver) {
        super(driver);
    }


    //methods
    public void waitForPageToLoad() {
        waitForPage.until(ExpectedConditions.visibilityOf(siteFooter));
    }

    public void verifyWhatIsSeleniumTextIsPresent(String expectedText) {
        Assert.assertEquals(headerText.getText(), expectedText, "Something went wrong");
    }

    public void clickOnProjectsTitle() {
        waitForPage.until(ExpectedConditions.elementToBeClickable(seleniumProjectsTitle));
        seleniumProjectsTitle.click();
    }

    public void clickOnDocumentationTitle() {
        waitForPage.until(ExpectedConditions.elementToBeClickable(seleniumDocumentationTitle));
        seleniumDocumentationTitle.click();
    }

    public void clickOnSupportTitle() {
        waitForPage.until(ExpectedConditions.elementToBeClickable(GettingHelpTitle));
        GettingHelpTitle.click();
    }

    public void clickOnAboutTitle() {
        waitForPage.until(ExpectedConditions.elementToBeClickable(AboutSeleniumTitle));
        AboutSeleniumTitle.click();
    }

    public void clickOnDownloadTitle() {
        waitForPage.until(ExpectedConditions.elementToBeClickable(DownloadsTitle));
        DownloadsTitle.click();
    }
}
