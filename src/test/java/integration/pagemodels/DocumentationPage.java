package integration.pagemodels;


import integration.configuration.AbstractWebComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class DocumentationPage extends AbstractWebComponent {
        //locators
        @FindBy(css = "#selenium-documentation>h1")
        private WebElement headerText1;

    public DocumentationPage(WebDriver driver) {
        super(driver);
    }

    public void verifySeleniumDocumentationTextIsPresent(String expectedText) {
        Assert.assertEquals(headerText1.getText(), expectedText, "Something went wrong");

    }
}
