package integration.test;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class recorderTestSelenium {

    @Test
    public void recorderTestSelenium() {
        WebDriver driver = new FirefoxDriver();

        //HW 8.1

        driver.get("http://testerstories.com/test_app/test_iframe.html");
        driver.switchTo().frame("static");
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("document.querySelector('#avengers_assemble_id').click()");
        Assert.assertEquals(driver.findElement(By.tagName("h1")).getText(), "Success 1");
        Assert.assertEquals(driver.findElement(By.tagName("p")).getText(), "This is the first success page.");
        driver.switchTo().defaultContent();
        driver.close();
    }

}


