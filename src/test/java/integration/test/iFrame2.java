package integration.test;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;


public class iFrame2 {

    @Test
    public void iFrame2() {
        WebDriver driver = new FirefoxDriver();

        //HW 8.2

        //1) Goto http://testerstories.com/test_app/test_iframe.html;
        driver.get("http://testerstories.com/test_app/test_iframe.html");

        //2) At second window type:
        //- Name: “Selenium”
        //- E-mail: qwe@dd.com
        //- Message: “test_test_test”
        driver.switchTo().frame("form");
        driver.findElement(By.id("name")).clear();
        driver.findElement(By.id("name")).sendKeys("Selenium");
        driver.findElement(By.id("email")).clear();
        driver.findElement(By.id("email")).sendKeys("qwe@dd.com");
        driver.findElement(By.id("message")).clear();
        driver.findElement(By.id("message")).sendKeys("test_test_test");

        //3) Click at ‘Submit Form’ button (use JavascriptExecutor);
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("document.querySelector('#theform p input.submit').click()");

        //4) Verify ‘Success 1 This is the first success page.’ text is visible;
        Assert.assertEquals(driver.findElement(By.tagName("h1")).getText(), "Success 1");
        Assert.assertEquals(driver.findElement(By.tagName("p")).getText(), "This is the first success page.");

        //5) Click navigate back on your browser;
        driver.navigate().back();

        //6) Modify ‘Message’ field to “web driver test text” and submit the form one’s again;
        driver.findElement(By.id("message")).clear();
        driver.findElement(By.id("message")).sendKeys("web driver test text");
        JavascriptExecutor jse1 = (JavascriptExecutor) driver;
        jse1.executeScript("document.querySelector('#theform p input.submit').click()");

        //7) Verify ‘Success 1 This is the first success page.’ text is visible;
        Assert.assertEquals(driver.findElement(By.tagName("h1")).getText(), "Success 1");
        Assert.assertEquals(driver.findElement(By.tagName("p")).getText(), "This is the first success page.");
        driver.switchTo().defaultContent();
        driver.close();
    }
}

