package integration.pagemodels;


import integration.configuration.AbstractWebComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class ProjectsPage extends AbstractWebComponent {
        //locators
        @FindBy(css = "#mainContent h2:nth-child(1)")
        private WebElement headerText;

    public ProjectsPage(WebDriver driver) {
        super(driver);
    }

    public void verifySeleniumProjectsTextIsPresent(String expectedText) {
        Assert.assertEquals(headerText.getText(), expectedText, "Something went wrong");

    }
}
