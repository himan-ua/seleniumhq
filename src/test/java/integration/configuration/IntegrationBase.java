package integration.configuration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class IntegrationBase {
    public static final String BASIC_URL;
    public static final Integer DEFAULT_WAITFOR_PAGE_SECONDS;
    public static final Integer DEFAULT_WAITFOR_AJAX_SECONDS;

    static {
        BASIC_URL = "http://www.seleniumhq.org/";
        DEFAULT_WAITFOR_PAGE_SECONDS = 30;
        DEFAULT_WAITFOR_AJAX_SECONDS = 30;
    }

    protected WebDriver driver;
    protected String browser;


    public IntegrationBase(String browser) {
        this.browser = browser;
    }

    @BeforeMethod
    public final void setup() {
        driver = createWebDriver();
    }

    private WebDriver createWebDriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\adminn\\Documents\\Work\\Selenide\\chromedriver.exe");
        driver = new ChromeDriver();
        //driver = new FirefoxDriver();
        return driver;
    }

    public void openBasicHomePage(String url) {
        driver.navigate().to(url);
        //waitUntilPageToLoad();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    @AfterMethod
    public final void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
