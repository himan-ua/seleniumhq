package integration.pagemodels;


import integration.configuration.AbstractWebComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class AboutPage extends AbstractWebComponent {
    //locators
    @FindBy(css = "#mainContent h2:nth-child(1)")
    private WebElement headerText3;

    public AboutPage(WebDriver driver) {
        super(driver);
    }

    public void verifyAboutSeleniumTextIsPresent(String expectedText) {
        Assert.assertEquals(headerText3.getText(), expectedText, "Something went wrong");

    }
}
