package integration.pagemodels;


import integration.configuration.AbstractWebComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class SupportPage extends AbstractWebComponent {
        //locators
        @FindBy(css = "#mainContent>h1")
        private WebElement headerText2;

        @FindBy(css = "#mainContent>h2")
        private WebElement bodyText1;

        @FindBy(css = "#IRC")
        private WebElement bodyText2;

        @FindBy(css = "#BugTracker")
        private WebElement bodyText3;

        @FindBy(css = "#CommercialSupport")
        private WebElement bodyText4;

    public SupportPage(WebDriver driver) {
        super(driver);
    }

    public void verifyGettingHelpTextIsPresent(String expectedText) {
        Assert.assertEquals(headerText2.getText(), expectedText, "Something went wrong");
    }

    public void verifyUserGroupTextIsPresent(String expectedText) {
        Assert.assertEquals(bodyText1.getText(), expectedText, "Something went wrong");
    }

    public void verifyChatRoomTextIsPresent(String expectedText) {
        Assert.assertEquals(bodyText2.getText(), expectedText, "Something went wrong");
    }

    public void verifyBugTrackerTextIsPresent(String expectedText) {
        Assert.assertEquals(bodyText3.getText(), expectedText, "Something went wrong");
    }

    public void verifyCommercialSupportTextIsPresent(String expectedText) {
        Assert.assertEquals(bodyText4.getText(), expectedText, "Something went wrong");
    }
}
