package integration.pagemodels;


import integration.configuration.AbstractWebComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class DownloadPage extends AbstractWebComponent {
    //locators
    @FindBy(css = "#mainContent h2:nth-child(1)")
    private WebElement headerText4;

    public DownloadPage(WebDriver driver) {
        super(driver);
    }

    public void verifyDownloadsTextIsPresent(String expectedText) {
        Assert.assertEquals(headerText4.getText(), expectedText, "Something went wrong");

    }
}
